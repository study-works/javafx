package display;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;

import java.awt.*;

/**
 * Class to display backgrounds
 */
public class DisplayBackground {
    /**
     * Get the screen size
     */
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    /**
     * Get the ratio by means of the screen size
     */
    private static double RATIO = screenSize.getWidth()/screenSize.getHeight();

    /**
     * Display the background
     * @param iV Background image
     * @param s The background scene
     * @param g The group
     */
    public static void backgroundDisplay(ImageView iV, Scene s, Group g) {
        When condition = Bindings.when((s.widthProperty().divide(s.heightProperty())).greaterThanOrEqualTo(RATIO));
        iV.fitWidthProperty().bind(condition.then(iV.fitHeightProperty().multiply(RATIO)).otherwise(s.widthProperty()));
        iV.fitHeightProperty().bind(condition.then(s.heightProperty()).otherwise(iV.fitWidthProperty().divide(RATIO)));

        g.layoutXProperty().bind((s.widthProperty().subtract(iV.fitWidthProperty())).divide(2));
        g.layoutYProperty().bind((s.heightProperty().subtract(iV.fitHeightProperty())).divide(2));
    }

    /**
     * display a block
     * @param iV the block image
     * @param background the background image
     * @param LRatio The width ration
     * @param HRatio The height ratio
     * @param hGX The X location (width)
     * @param hGY The Y location (height)
     */
    public static void blockDisplay(ImageView iV, ImageView background, double LRatio, double HRatio,SimpleDoubleProperty hGX, SimpleDoubleProperty hGY) {
        iV.fitHeightProperty().bind(background.fitHeightProperty().multiply(HRatio));
        iV.fitWidthProperty().bind(background.fitWidthProperty().multiply(LRatio));
        iV.layoutXProperty().bind(background.fitWidthProperty().multiply(hGX));
        iV.layoutYProperty().bind(background.fitHeightProperty().multiply(hGY));
    }
}

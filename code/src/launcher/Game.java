package launcher;

import control.GameControl;
import control.LevelControl;
import display.DisplayCharacter;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.characters.CharacterMover;
import model.characters.Enemy;
import model.characters.PlayerCharacter;
import model.level.Block;
import model.level.BlockManager;
import model.level.Level;
import model.weapons.Weapon;
import model.weapons.WeaponShoot;

import java.util.List;

/**
 * Launch the game
 */
public class Game {
    private static final String SKIN_ENEMY = "/texture/skin/character/enemyTest.jpg";
    private static final double L_RATIO = 0.046875d;
    private static final double H_RATIO = 0.166666d;
    private static final double START_X = 0.1d;


    private static double SPEEDY = 0;
    private final static double JUMPSPEED = 0.012d;
    private final static double G = 0.00019d;
    private static double GROUND_LEVEL = 0.9d;
    /**
     * Player character 1
     * @see PlayerCharacter
     */
    private PlayerCharacter character1;
    /**
     * Player character 2
     * @see PlayerCharacter
     */
    private PlayerCharacter character2;
    /**
     * The level of the game
     */
    private Level level;
    private  LevelControl levelControl;
    /**
     * Initialize the game
     * @param character1 player characters 1
     * @param character2 player characters 2 (if 2 player)
     */
    public Game(PlayerCharacter character1, PlayerCharacter character2) {
        this.character1 = character1;
        this.character2 = character2;
        levelControl = new LevelControl();
    }
    /**
     * Add player character in the list
     * @deprecated the list is passed as a parameter of the constructor
     * @param name The name of the character
     * @param life The life of the character
     * @param weaponList The weapons of the character
     * @param skin The skin of the character
     */
    @Deprecated
    public void characterLoader(String name, int life, List<Weapon> weaponList, ImageView skin) {
//        PlayerCharacter p = new PlayerCharacter(name, life, weaponList, skin);
//        characters.add(p);
    }

    /**
     * Load the game
     * @param s Parent stage
     */
    public void gameLoader(Stage s) {
        try {
            Group root = new Group();
            Scene scene = new Scene(root);
            s.setScene(scene);
            level = levelControl.levelLoader(scene, root);
            Enemy enemy = new Enemy("jacko THE TERROR", 10, null, new ImageView(new Image(SKIN_ENEMY)), 0.3d, GROUND_LEVEL - H_RATIO);

            character1.setX(new SimpleDoubleProperty(START_X));
            character1.setY(new SimpleDoubleProperty(GROUND_LEVEL));
            enemy.setX(new SimpleDoubleProperty(enemy.getBaseX()));
            enemy.setY(new SimpleDoubleProperty(enemy.getBaseY()));

            DisplayCharacter.characterDisplay(character1.getSkin(), L_RATIO, H_RATIO, character1.getX(), character1.getY(), level.getBackground());
            DisplayCharacter.characterDisplay(enemy.getSkin(), L_RATIO, H_RATIO, enemy.getX(), enemy.getY(), level.getBackground());

            AnimationTimer loop = new AnimationTimer() {
                @Override
                public void handle(long arg0) {
                    if (character1.getSkin().getLayoutY() + character1.getSkin().getFitHeight() <= level.getBackground().getFitHeight() * GROUND_LEVEL) {
                        character1.getY().set(character1.getY().get() + SPEEDY);
                        SPEEDY += G;
                    }
                    if (character1.getSkin().getLayoutY() + character1.getSkin().getFitHeight() > level.getBackground().getFitHeight() * GROUND_LEVEL) {
                        character1.getY().set((level.getBackground().getFitHeight() * GROUND_LEVEL - character1.getSkin().getFitHeight()) / level.getBackground().getFitHeight());
                        SPEEDY = 0;
                    }

                    if (character1.getWalk()) {
                        CharacterMover.move(character1);
                    }
                    if(character1.getWeapon().getShoot()) {
                        WeaponShoot.shoot(character1, root,enemy);
                    }
                    if (character1.getJump()) {
                        SPEEDY = -JUMPSPEED;
                        CharacterMover.jump(character1, SPEEDY);
                        character1.setJump(false);
                    }
                    WeaponShoot.enemyAttack(character1, enemy);
                    if (enemy.getLife() == 0 || character1.getLife() == 0) {
                        s.close();
                    }
                    GROUND_LEVEL = levelControl.groundControl(level.getBlocs(), character1);
                }
            };
            loop.start();
            GameControl.control(scene, character1, level.getBackground(), root, s, loop);
            root.getChildren().addAll(level.getBackground(), character1.getSkin(), enemy.getSkin());
            for(Block block : level.getBlocs()) {
                root.getChildren().add(block.getBlockSkin());
            }
            BlockManager blockManager = new BlockManager();
            blockManager.floorLoading(scene, root, level.getBackground());
            scene.setFill(Color.BLACK);
            scene.setCursor(Cursor.NONE);
            s.setFullScreen(true);
            s.setFullScreenExitHint("");
            s.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
            s.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}

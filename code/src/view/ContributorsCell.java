package view;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.contributors.Contributor;
import model.contributors.Contributors;
import model.contributors.Stub;

public class ContributorsCell {

    @FXML
    private ListView<Contributor> listContributor;

    private Contributors contributors;

    @FXML
    private Label contributorName;

    @FXML
    private Label contributorFirstName;

    @FXML
    private Label contributorGroup;

    public void initialize() {

        contributors = Stub.generateContributors();

        listContributor.itemsProperty().bind(contributors.contributorsProperty());

        listContributor.setCellFactory((param) ->
                new ListCell<Contributor>() {
                    @Override
                    protected void updateItem(Contributor item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            textProperty().bind(item.firstNameProperty());
                        } else {
                            textProperty().unbind();
                            setText("");
                        }
                    }
                });

        listContributor.getSelectionModel().selectedItemProperty().addListener((__,old,newV)-> {
            if (old != null) {
                contributorFirstName.textProperty().unbind();
                contributorName.textProperty().unbind();
                contributorGroup.textProperty().unbind();
            }
            if (newV != null) {
                contributorFirstName.textProperty().bind(newV.firstNameProperty());
                contributorName.textProperty().bind(newV.nameProperty());
                contributorGroup.textProperty().bind(newV.groupProperty());
            }
        });
    }
}
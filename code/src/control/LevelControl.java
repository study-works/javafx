package control;

import display.DisplayBackground;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.characters.PlayerCharacter;
import model.level.Block;
import model.level.BlockManager;
import model.level.Level;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller of a Level
 */
public class LevelControl {

    private static final double L_RATIO = 0.046875d;
    private static final double H_RATIO = 0.166666d;
    private static final double H_RATIO_BLOCK = 0.086875d;
    private static final double L_RATIO_BLOCK = 0.046875d;
    private static final String BACKGROUND_IMAGE = "/Image/backgroundtest.png";
    /**
     * Get the screen size
     */
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public LevelControl() {

    }

    /**
     * set the new ground level for the character
     * @param l the list of all blocks on the map
     * @param p the playercharacter who's moving
     * @return the new ground level for he character
     */
    public double groundControl (List<Block> l, PlayerCharacter p) {
        double ground_level = 0.9d;
        for(Block block : l) {
                if (p.getX().get() + L_RATIO >= block.getCoordonateX() && p.getX().get() <= block.getCoordonateX() + L_RATIO_BLOCK) {
                    if (p.getY().get() + H_RATIO <= block.getCoordonateY()) {
                        ground_level = block.getCoordonateY();
                    }
                    else {
                       ground_level = 0.9d;
                    }
                }
            }
        return ground_level;
    }

    /**
     * Load the level with all the blocks
     * @param scene the game scene
     * @param root the group of our scene
     * @return return the loaded level
     */
    public Level levelLoader(Scene scene, Group root) {
        List<Block> blocks = new ArrayList<>();
        BlockManager blockManager = new BlockManager();
        blocks = blockManager.blockLoading();
        Level level = new Level(screenSize.getHeight(),screenSize.getWidth(), blocks, null, new ImageView(new Image(BACKGROUND_IMAGE)));
        DisplayBackground.backgroundDisplay(level.getBackground(), scene, root);
        for(Block b : blocks) {
            DisplayBackground.blockDisplay(b.getBlockSkin(), level.getBackground(), L_RATIO_BLOCK, H_RATIO_BLOCK, new SimpleDoubleProperty(b.getCoordonateX()), new SimpleDoubleProperty(b.getCoordonateY()));
        }
        return level;
    }
}

package model.weapons;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.image.ImageView;

/**
 * The general weapon class
 */
public abstract class Weapon {
    /**
     * The name of the weapon
     */
    private String name;
    /**
     * The damage of the weapon
     */
    private int damage;
    /**
     * The range of the weapon
     */
    private int range;
    /**
     * The attack texture of the weapon
     */
    private ImageView attackTexture;
    /**
     * Indicates whether the character is shooting or not
     * By default, he don't jump
     */
    private boolean shoot = false;
    private SimpleDoubleProperty x = new SimpleDoubleProperty();
    private SimpleDoubleProperty y = new SimpleDoubleProperty();

    /**
     * Construct the weapon
     * @param name The name of the weapon
     * @param damage The damage caused by the weapon
     * @param range The range of the weapon
     * @param attackTexture The attack texture of the weapon (image)
     */
    public Weapon(String name, int damage, int range, ImageView attackTexture) {
        this.name = name;
        this.damage = damage;
        this.range = range;
        this.attackTexture = attackTexture;
    }

    public String getName() { return name; }
    private void setName(String name) { this.name = name; }

    public int getDamage() { return damage; }
    private void setDamage(int damage) { this.damage = damage; }

    public int getRange() { return range; }
    private void setRange(int range) { this.range = range; }

    public ImageView getAttackTexture() { return attackTexture; }
    public void setAttackTexture(ImageView attackTexture) { this.attackTexture = attackTexture; }

    public SimpleDoubleProperty getX() { return  x; }
    public void setX(SimpleDoubleProperty x) {this.x = x;}

    public SimpleDoubleProperty getY() { return  y; }
    public void setY(SimpleDoubleProperty y) {this.y = y;}

    public boolean getShoot() { return shoot; }
    public void setShoot(boolean shoot) { this.shoot = shoot;}
}

package model.level;

import display.DisplayBackground;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Manager of blocks
 */
public class BlockManager {
    private static final String TEXTURE_DIRT = "/texture/bloc/dirt.jpg";
    private static final double L_RATIO_BLOCK = 0.046875d;
    private static final double H_RATIO_BLOCK = 0.086875d;

    public BlockManager() {}

    /**
     * load the blocks
     * @return the list of loaded blocks
     */
    public List<Block> blockLoading() {
        List<Block> blocks = new ArrayList<>();
        int i = 1;
        Block b = new Dirt(0.96d, 0.6d, new ImageView(new Image(TEXTURE_DIRT)));
        blocks.add(b);
        while(i < 8) {
            b = new Dirt(0.96d, 0.6d, new ImageView(new Image(TEXTURE_DIRT)));
            b.setCoordonateX(b.getCoordonateX() - L_RATIO_BLOCK * i);
            blocks.add(b);
            i++;
        }
        b = new Dirt(0.5d, 0.4d, new ImageView(new Image(TEXTURE_DIRT)));
        blocks.add(b);
        return  blocks;
    }

    /**
     * load the floor of the level
     * @param scene scene of the game
     * @param root group of the scene
     * @param background image of the background
     */
    public void floorLoading(Scene scene, Group root, ImageView background) {
        int i = 1;
        Block b = new Dirt(-0.04, 0.9d, new ImageView(new Image(TEXTURE_DIRT)));
        DisplayBackground.blockDisplay(b.getBlockSkin(), background, L_RATIO_BLOCK, H_RATIO_BLOCK, new SimpleDoubleProperty(b.getCoordonateX()), new SimpleDoubleProperty(b.getCoordonateY()));
        root.getChildren().add(b.getBlockSkin());
        while(b.getCoordonateX() <= 0.96d) {
            b = new Dirt(-0.04, 0.9d, new ImageView(new Image(TEXTURE_DIRT)));
            b.setCoordonateX(b.getCoordonateX() + L_RATIO_BLOCK * i);
            DisplayBackground.blockDisplay(b.getBlockSkin(), background, L_RATIO_BLOCK, H_RATIO_BLOCK, new SimpleDoubleProperty(b.getCoordonateX()), new SimpleDoubleProperty(b.getCoordonateY()));
            i++;
            root.getChildren().add(b.getBlockSkin());
        }
    }
}

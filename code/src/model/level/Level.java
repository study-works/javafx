package model.level;

import javafx.scene.image.ImageView;
import model.characters.Enemy;

import java.util.Collection;
import java.util.List;

/**
 * The level class
 */
public class Level {
    /**
     * The height of the level
     * Number of blocks
     */
    private double height;
    /**
     * The width of the level
     * Number of blocks
     */
    private double width;
    /**
     * List of blocks making up the level
     * @see Block
     */
    private List<Block> blocs;
    /**
     * List of all enemies in a level
     * @see Enemy
     */
    private Collection<Enemy> enemies;
    /**
     * Background of the level
     */
    private ImageView background;

    /**
     * Construct the level
     * @param height Height of the level
     * @param width Width of the level
     * @param blocs List of all blocks of the level
     */
    public Level(double height, double width, List<Block> blocs, Collection<Enemy> enemies, ImageView background) {
        this.height = height;
        this.width = width;
        this.blocs = blocs;
        this.enemies = enemies;
        this.background = background;
    }

    public double getHeight() { return height; }
    private void setHeight(double height) { this.height = height; }

    public double getWidth() { return width; }
    private void setWidth(double width) { this.width = width; }

    public List<Block> getBlocs() { return blocs; }
    private void setBlocs(List<Block> blocs) { this.blocs = blocs; }

    public ImageView getBackground() {return background;}
    public void setBackground(ImageView background) {
        this.background = background;
    }
}

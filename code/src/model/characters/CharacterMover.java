package model.characters;

/**
 * Manages a character's movements
 */
public class CharacterMover {

    private static final String RIGHT = "right";
    private static final String LEFT = "left";
    private static final double V_WALK = 0.002d;
    private static final double L_WALL = 0.0009d;
    private static final double R_WALL = 0.953d;

    public CharacterMover() {

    }

    /**
     * Blow up the character
     * @param character The character to be blown up
     * @param speedY The speed of the jump
     */
    public static void jump(Character character, double speedY) {
        character.getY().set(character.getY().get() + speedY);
    }

    /**
     * Moves a player character
     * @param character The character to be moved
     */
    public static void move(Character character) {
        if (character.getDirectionView().equals(RIGHT))
        {
           if (character.getX().get() < R_WALL) {
                character.getSkin().setScaleX(1);
                character.getX().set(character.getX().get() + V_WALK);
           }
        }
        if (character.getDirectionView().equals(LEFT)) {
            if (character.getX().get() > L_WALL) {
                character.getSkin().setScaleX(-1);
                character.getX().set(character.getX().get() - V_WALK);
            }
        }
    }

    /**
     * Moves automatically an enemy
     * @param enemy The enemy to be moved
     */
    public static void enemyMove(Character enemy) {
        if (enemy.getDirectionView().equals(RIGHT)) {
            if (enemy.getX().get() <= R_WALL) {
                enemy.getSkin().setScaleX(-1);
                enemy.getX().set(enemy.getX().get() + V_WALK);
            }
            else { enemy.setDirectionView(LEFT);}
        }
        if (enemy.getDirectionView().equals(LEFT)) {
            if (enemy.getX().get() >= L_WALL) {
                enemy.getSkin().setScaleX(1);
                enemy.getX().set(enemy.getX().get() - V_WALK);
            }
            else { enemy.setDirectionView(RIGHT);}
        }
    }
}

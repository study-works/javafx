package model.characters;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.persistance.Savior;
import model.persistance.SaviorTxt;
import model.weapons.Gun;

import java.util.Random;
import java.util.TreeSet;

/**
 * Manage the choice of a player character
 * Randomly when start a game
 */
public class CharacterManager {
    private static final String SKIN_BALLE = "/texture/skin/balle.png";
    private static final String SKIN_CHARACTER_TEST = "/texture/skin/character/characterTest.jpg";
    private static final String SKIN_CHARACTER_RAMBRO = "/texture/skin/character/rambro.png";
    /**
     * List of all available playable characters
     * @see PlayerCharacter
     */
    private TreeSet<PlayerCharacter> playerCharacters;

    /**
     * Constructor of the character manager and save all the characters names in a file
     */
    public CharacterManager() {
        playerCharacters = new TreeSet<>();
        this.playerCharacters.add(new PlayerCharacter("rocky", 10, new Gun("magnum", 2, 10, new ImageView(new Image(SKIN_BALLE))), null, new ImageView(new Image(SKIN_CHARACTER_TEST))));
        this.playerCharacters.add(new PlayerCharacter("neo", 15, new Gun("revolver", 1, 10, new ImageView(new Image(SKIN_BALLE))), null, new ImageView(new Image(SKIN_CHARACTER_RAMBRO))));
        playerCharacters.forEach(playerCharacter -> System.out.println(playerCharacter.getName()));
        //playerCharacters.forEach(System.out::println);
        String FILE_NAME = "Name.txt";
        Savior savior = new SaviorTxt(FILE_NAME);
        savior.save(playerCharacters);

    }

    public TreeSet<PlayerCharacter> getPlayerCharacter() { return playerCharacters; }
    public void setPlayerCharacters(TreeSet<PlayerCharacter> playerCharacters) { this.playerCharacters = playerCharacters; }

    /**
     * Choose randomly a character in the list
     * @return The playable character for the player
     */
    public PlayerCharacter getRandomPlayerCharacter() {
        int size = playerCharacters.size();
        int item = new Random().nextInt(size); // In real life, the Random object should be rather more shared than this
        int i = 0;
        for(PlayerCharacter playerCharacter : playerCharacters)
        {
            if (i == item)
                return playerCharacter;
            i++;
        }
        return null;
    }
}

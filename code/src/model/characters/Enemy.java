package model.characters;

import javafx.scene.image.ImageView;
import model.weapons.Weapon;

/**
 * The enemy class
 * Extends Character
 * @see Character
 */
public class Enemy extends Character {
    private final double baseX;
    private final double baseY;
    private boolean isAttacking = false;

    /**
     * Construct an enemy
     * @param name The name of the enemy
     * @param life The life of the enemy
     * @param weapon The weapon of the enemy
     * @param skin The skin of the enemy (image)
     * @param baseX The X location
     * @param baseY The Y location
     */
    public Enemy(String name, int life, Weapon weapon, ImageView skin, double baseX, double baseY) {
        super(name, life, weapon, skin);
        this.baseX = baseX;
        this.baseY = baseY;
    }

    public double getBaseX() {return baseX;}
    public double getBaseY() {return baseY;}
    public boolean getIsAttacking() {return isAttacking;}
    public void setIsAttacking(boolean attacking) { isAttacking = attacking; }
}

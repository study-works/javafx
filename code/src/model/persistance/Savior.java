package model.persistance;

import model.characters.PlayerCharacter;

import java.util.TreeSet;

/**
 * Saving class
 */
public abstract class Savior {
    Savior() {}
    public abstract void save(TreeSet<PlayerCharacter> p);
}

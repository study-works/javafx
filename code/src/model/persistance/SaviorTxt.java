package model.persistance;

import model.characters.PlayerCharacter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.TreeSet;

/**
 * Saving class for txt
 */
public class SaviorTxt extends Savior {
    private String file_name;
    public SaviorTxt(String name) {
        file_name = name;
    }

    /**
     * Save all the name of the character in the Tree
     * @param p TreeSet of PlayerCharacter
     */
    @Override
    public void save(TreeSet<PlayerCharacter> p) {
        for(PlayerCharacter data : p) {
            byte[] bytes = data.getName().getBytes();
            try (OutputStream out = new FileOutputStream(file_name)) {
                out.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

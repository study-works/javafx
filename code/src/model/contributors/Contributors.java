package model.contributors;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Collection of contributors
 * The collections are observable
 */
public class Contributors {

    private ObservableList<Contributor> contributorsObs = FXCollections.observableArrayList();
    private final ListProperty<Contributor> contributors = new SimpleListProperty<>(contributorsObs);
    public ObservableList<Contributor> getContributors() {return contributors.get();}
    public void setContributors(ObservableList<Contributor> value) {contributors.set(value);}
    public ListProperty<Contributor> contributorsProperty() {return contributors;}

    /**
     * Construct of the collection
     * @param contributor A contributor to add to the collection
     */
    void addcontributor(Contributor contributor) {
        contributorsObs.add(contributor);
    }
}

package model.contributors;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The contributors class
 */
public class Contributor {

    /**
     * Name of the contributor
     */
    private final StringProperty name = new SimpleStringProperty();
    public String getName() {return name.get();}
    public StringProperty nameProperty() {return name;}
    public void setName(String name) {this.name.set(name);}

    /**
     * First Name of the contributor
     */
    private final StringProperty firstName = new SimpleStringProperty();
    public String getFirstName() {return firstName.get();}
    public StringProperty firstNameProperty() {return firstName;}
    public void setFirstName(String firstName) {this.firstName.set(firstName);}

    /**
     * Group in the IUT of the contributor
     */
    private final StringProperty group = new SimpleStringProperty();
    public String getGroup() {return group.get();}
    public StringProperty groupProperty() {return group;}
    public void setGroup(String group) {this.group.set(group);}

    /**
     * Construct a contributor
     * @param name Name of the contributor
     * @param firstName First Name of the contributor
     * @param group Group in the IUT of the contributor
     */
    Contributor(String name, String firstName, String group) {
        this.name.set(name);
        this.firstName.set(firstName);
        this.group.set(group);
    }
}
